FROM raspbian/stretch:latest

MAINTAINER Amael Heiligtag "amael@heiligtag.com"

ENV DEBIAN_FRONTEND=noninteractive \
    OV_PASSWORD=admin \
    PUBLIC_HOSTNAME=openvas

# First install all packages
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install openvas bzip2 net-tools nmap openssh-client nsis rpm alien curl -yq && \
    rm -rf /var/cache/apk/* && \
    rm -rf /var/lib/apt/lists/*

# Then copy files
COPY config/redis.conf /etc/redis/redis.conf
#COPY config/sasl_passwd_template /
#COPY config/main.cf_template /
#COPY config/ldapUserSync/* /ldapUserSync/
COPY start /start


RUN mkdir -p /var/run/redis && \
    wget -q --no-check-certificate https://svn.wald.intevation.org/svn/openvas/branches/tools-attic/openvas-check-setup -O /openvas-check-setup && \
    chmod +x /openvas-check-setup && \
    sed -i 's/MANAGER_ADDRESS=127.0.0.1/MANAGER_ADDRESS=0.0.0.0/' /etc/default/openvas-manager && \
    sed -i 's/DODTIME=.*/DODTIME=300/' /etc/default/openvas-manager && \
    sed -i 's/GSA_ADDRESS=127.0.0.1/GSA_ADDRESS=0.0.0.0/' /etc/default/greenbone-security-assistant && \
    sed -i 's/GSA_PORT=.*/GSA_PORT=443/' /etc/default/greenbone-security-assistant

# Run setup
RUN openvas-mkcert -q && \
    openvas-mkcert-client -n -i

RUN openvas-nvt-sync && \
    openvas-scapdata-sync && \
    openvas-certdata-sync

RUN BUILD=true /start && \
    service openvas-scanner stop && \
    service openvas-manager stop && \
    service greenbone-security-assistant stop && \
    service redis-server stop && \
    rm -f /var/lib/openvas/mgr/tasks.db

ENV BUILD=""

CMD /start

EXPOSE 443 9390
